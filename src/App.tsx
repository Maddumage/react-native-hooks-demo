import React from 'react';
import {SafeAreaView, StyleSheet, View, Text, StatusBar} from 'react-native';
import Store from './store/Store';
import Button from './components/Button/Button';
import CountView from './components/CountView/CountView';
import UserView from './components/UserView/UserView';

const App = () => {
  return (
    <Store>
      <StatusBar barStyle="dark-content" />
      <SafeAreaView>
        <View style={styles.container}>
          <Text style={styles.textTitle}>hooks-contextapi-demo</Text>
        </View>
        <CountView />
        <Button type="ADD" />
        <Button type="MINUS" />
        <UserView />
      </SafeAreaView>
    </Store>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#fff',
    justifyContent: 'center',
    alignItems: 'center',
    padding: 16,
  },
  textTitle: {
    color: 'green',
    fontSize: 18,
    fontWeight: 'bold',
  },
});

export default App;
