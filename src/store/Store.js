import React, {createContext, useReducer} from 'react';
import Reducer from '../reducer/Reducer';

const initialState = {
  users: [],
  count: 0,
  company: 'Unicorn',
};

const Store = ({children}) => {
  const [state, dispatch] = useReducer(Reducer, initialState);
  return (
    <Context.Provider value={[state, dispatch]}>{children}</Context.Provider>
  );
};

// export global context(state) creating a new global context
export const Context = createContext(initialState);
export default Store;
// export state to use globaly
