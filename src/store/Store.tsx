// import React, {createContext, useReducer} from 'react';
// import {Reducer, initialState} from '../reducer/Reducer';

// type ContextProps = {
//   count: number;
//   company: string;
// };

// // define store
// const Store: React.FC = ({children}) => {
//   const [state, dispatch] = useReducer(Reducer, initialState);
//   return (
//     <Context.Provider value={[state, dispatch]}>{children}</Context.Provider>
//   );
// };

// // export global context(state) creating a new global context
// export const Context = createContext<Partial<ContextProps>>({});
// export default Store;
