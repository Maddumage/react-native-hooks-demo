export const incrementCount = count => {
  return {
    type: 'INCREMENT',
    payload: count,
  };
};
