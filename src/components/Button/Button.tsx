import React, {Children} from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import {Context} from '../../store/Store';

export const Button = ({type = 'ADD', text = '+1'}) => {
  const [state, dispatch] = React.useContext(Context);
  if (!state) {
    throw Error;
  }
  return (
    <TouchableOpacity
      style={{
        backgroundColor: type === 'ADD' ? 'green' : 'red',
        width: 100,
        height: 48,
        borderRadius: 5,
        alignItems: 'center',
        justifyContent: 'center',
        alignSelf: 'center',
        margin: 16,
      }}
      onPress={() => {
        if (type === 'ADD') {
          dispatch({
            type: 'INCREMENT',
            payload: 1,
          });
        } else {
          dispatch({
            type: 'DECREMENT',
            payload: 1,
          });
        }
      }}>
      <Text style={{color: '#fff', fontSize: 16, fontWeight: 'bold'}}>
        {type === 'ADD' ? '+1' : '-1'}
      </Text>
    </TouchableOpacity>
  );
};

export default Button;
