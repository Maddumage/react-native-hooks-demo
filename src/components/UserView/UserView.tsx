import React from 'react';
import {View, Text} from 'react-native';
import {Context} from '../../store/Store';
import Button from '../Button/Button';

//export const storeContext = React.createContext<TStore|null>(null);
//const storeContext = React.createContext<TStore | null | userDTO[]>(users);

interface userDTO {
  name: string;
  address: string;
}

export const UserView: React.FC<userDTO> = () => {
  const [state, dispatch] = React.useContext(Context);
  if (!state) {
    throw Error;
  }
  return (
    <View style={{alignSelf: 'center'}}>
      <Text style={{fontSize: 16}}>
        name is {state.company} and {state.count} users
      </Text>
      <Button />
    </View>
  );
};

export default UserView;
