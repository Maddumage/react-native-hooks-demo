import React from 'react';
import {View, Text} from 'react-native';
import {Context} from '../../store/Store';

export const CountView = () => {
  const [state, dispatch] = React.useContext(Context);
  if (!state) {
    throw Error;
  }
  return (
    <View
      style={{
        backgroundColor: 'gray',
        width: 100,
        height: 100,
        borderRadius: 5,
        alignItems: 'center',
        justifyContent: 'center',
        alignSelf: 'center',
        margin: 16,
      }}>
      <Text style={{color: '#fff', fontSize: 44, fontWeight: 'bold'}}>
        {state.count}
      </Text>
    </View>
  );
};

export default CountView;
